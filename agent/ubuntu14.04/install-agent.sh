#!/usr/bin/env bash
#   Use this script to install FusionInventory Agent on Ubuntu 14.04
cd "$(dirname "$0")"
apt-get --assume-yes update
apt-get --assume-yes install fusioninventory-agent
apt-get --assume-yes install fusioninventory-agent-task-network
apt-get --assume-yes install fusioninventory-agent-task-esx
apt-get --assume-yes install fusioninventory-agent-task-deploy
# TODO backup before overwrite
cp --force *.crt /etc/fusioninventory
cp --force agent.cfg /etc/fusioninventory
cp --force fusioninventory-agent.conf /etc/init
service fusioninventory-agent restart
sleep 1s
tail /var/log/fusioninventory.log
