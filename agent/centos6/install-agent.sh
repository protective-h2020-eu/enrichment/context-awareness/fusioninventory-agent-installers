#!/usr/bin/env bash
#   Use this script to install FusionInventory Agent on Ubuntu 16.04
cd "$(dirname "$0")"
yum -y install epel-release
yum repolist
yum -y install fusioninventory-agent fusioninventory-agent-task-inventory
yum -y install fusioninventory-agent-task-network
yum -y install fusioninventory-agent-task-esx
yum -y install fusioninventory-agent-task-deploy
# TODO backup before overwrite
cp --force *.crt /etc/fusioninventory
cp --force agent.cfg /etc/fusioninventory
service fusioninventory-agent restart
service fusioninventory-agent status
sleep 1s
tail -n 50 /var/log/fusioninventory.log
