#!/usr/bin/env bash
#   Use this script to install FusionInventory Agent on Ubuntu 16.04
cd "$(dirname "$0")"
apt-get --assume-yes update
apt-get --assume-yes install fusioninventory-agent
apt-get --assume-yes install fusioninventory-agent-task-network
apt-get --assume-yes install fusioninventory-agent-task-esx
apt-get --assume-yes install fusioninventory-agent-task-deploy
# TODO backup before overwrite
cp --force *.crt /etc/fusioninventory
cp --force agent.cfg /etc/fusioninventory
cp --force fusioninventory-agent.service /etc/systemd/system
cp --force fusioninventory-agent /etc/default
systemctl enable fusioninventory-agent
systemctl restart fusioninventory-agent
systemctl status fusioninventory-agent
sleep 1s
tail /var/log/fusioninventory.log
