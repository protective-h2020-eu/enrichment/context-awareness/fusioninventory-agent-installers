FusionInventory Agent Installers
================================

Prerequisites
-------------
To start, make sure Git is installed on your system.

Build zip files containing installers
-------

## With SSL

Clone this repository, and checkout the master branch.

`cd` into the checked out folder.

Note that each installer is in a subfolder named after the target operating system.

Copy the intermediate ca cert used to verify and encrypt `https` traffic between the agent and `ca-fusioninventory` into each of these subfolders. Should match `*.crt` pattern.

Edit the `agent.cfg` file in each subfolder changing the following entries to suit your configuration:
- server
- user
- password
- ca-cert-file
- no-ssl-check
- httpd-trust

Run the following in a terminal to generate customised zip files. The zip files will be put in the dist folder. `ubuntu16.04` is an example of an os/folder name.

- Linux/macOS
```
./gradlew -Pos=ubuntu16.04 zipAgent
```
- Windows
```
gradlew -Pos=ubuntu16.04 zipAgent
```

### Install the Agent
-----------------

Copy the appropriate zip file to the target machine and unzip.

`cd` into the unzipped folder.

Run `./install-agent.sh`

## Without SSL

This follows the installation for Ubuntu 16.04 LTS without SSL:

1. Clone the repository
```
git clone https://gitlab.com/protective-h2020-eu/enrichment/context-awareness/fusioninventory-agent-installers.git
```

2. cd into the checked out folder.

3. Go to **fusioninventory-agent-installers/agent/ubuntu16.04**

4. Edit **agent.cfg**. Change server to:
```
server = http://PROTECTIVE_NODE_IP:8089/api/upload
```
5. Run the following in a terminal to generate customised zip files. The zip files will be put in the dist folder with the name `ubuntu16.04.zip`.
```
./gradlew -Pos=ubuntu16.04 zipAgent
```
6. Unzip the generated folder **ubuntu16.04.zip**.
7. Run **./install-agent.sh**.
8. Once the script is finished running you should see something like:
```
fusioninventory-agent.service - FusionInventory agent
   Loaded: loaded (/etc/systemd/system/fusioninventory-agent.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2019-03-11 14:38:15 GMT; 8ms ago

```
This means that the agent is running and has been configured to point at your Fusion Inventory Server (Protective Node).

---


